#!/bin/bash
GIT_LATEST_BRANCH="develop"
AWS_REGISTRY=381024191737.dkr.ecr.eu-west-1.amazonaws.com

function build-package() {
	local NAME=$(basename $(pwd))
	local REMOTE_REPO="$AWS_REGISTRY/$NAME"
	local DEPENDENCIES_TAG="$REMOTE_REPO:$CI_GIT_BRANCH-dependencies"

	# Build dependencies image first
	docker pull $DEPENDENCIES_TAG || true
	docker build \
		--target dependencies \
		--cache-from $DEPENDENCIES_TAG \
		-t $DEPENDENCIES_TAG \
		"."

	# Build runtime image next, using cache
	docker pull $REMOTE_REPO:$CI_GIT_BRANCH || true
	docker build \
		--cache-from $DEPENDENCIES_TAG \
		--cache-from $REMOTE_REPO:$CI_GIT_BRANCH \
		-t $REMOTE_REPO:$CI_GIT_BRANCH \
		-t $REMOTE_REPO:$CI_GIT_COMMIT_SHORT \
		"."

	# Push dependencies image to remote repository for caching purposes
	docker push $DEPENDENCIES_TAG

	# Push runtime images to remote repository
	docker push $REMOTE_REPO:$CI_GIT_BRANCH
	docker push $REMOTE_REPO:$CI_GIT_COMMIT_SHORT

	# develop branch always gets the latest tag
	if [ "$CI_GIT_BRANCH" = "$GIT_LATEST_BRANCH" ]; then
		docker tag $REMOTE_REPO:$CI_GIT_BRANCH latest;
		docker push $REMOTE_REPO:latest
	fi
}

# Execute pre-build script
source ./pre-build.sh

# Build all packages
PACKAGES=$(find packages/ -maxdepth 1 -mindepth 1)
for PACKAGE in $PACKAGES; do
	pushd $PACKAGE
	build-package
	popd
done
