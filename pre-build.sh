#!/bin/bash
export CI=true
export CODEBUILD=true

export CI_GIT_BRANCH=`git symbolic-ref HEAD --short 2>/dev/null`
if [ "$CI_GIT_BRANCH" == "" ] ; then
  CI_GIT_BRANCH=`git branch -a --contains HEAD | sed -n 2p | awk '{ printf $1 }'`
  export CI_GIT_BRANCH=${CI_GIT_BRANCH#remotes/origin/}
fi

export CI_GIT_MESSAGE=`git log -1 --pretty=%B`
export CI_GIT_AUTHOR=`git log -1 --pretty=%an`
export CI_GIT_AUTHOR_EMAIL=`git log -1 --pretty=%ae`
export CI_GIT_COMMIT=`git log -1 --pretty=%H`
export CI_GIT_COMMIT_SHORT=`git log -1 --pretty=%H | cut -c 1-7`
export CI_GIT_TAG=`git describe --tags --abbrev=0`

export CI_PULL_REQUEST=false
if [[ $CI_GIT_BRANCH == pr-* ]] ; then
  export CI_PULL_REQUEST=${CI_GIT_BRANCH#pr-}
fi

export CI_PROJECT=${CI_BUILD_ID%:$CI_LOG_PATH}
export CI_BUILD_URL=https://$AWS_DEFAULT_REGION.console.aws.amazon.com/codebuild/home?region=$AWS_DEFAULT_REGION#/builds/$CI_BUILD_ID/view/new

echo "==> CI Environment Variables:"
echo "==> CI = $CI"
echo "==> CODEBUILD = $CODEBUILD"
echo "==> CI_GIT_AUTHOR = $CI_GIT_AUTHOR"
echo "==> CI_GIT_AUTHOR_EMAIL = $CI_GIT_AUTHOR_EMAIL"
echo "==> CI_GIT_BRANCH = $CI_GIT_BRANCH "
echo "==> CI_GIT_COMMIT = $CI_GIT_COMMIT"
echo "==> CI_GIT_COMMIT_SHORT = $CI_GIT_COMMIT_SHORT"
echo "==> CI_GIT_MESSAGE = $CI_GIT_MESSAGE"
echo "==> CI_GIT_TAG = $CI_GIT_TAG"
echo "==> CI_PROJECT = $CI_PROJECT"
echo "==> CI_PULL_REQUEST = $CI_PULL_REQUEST"
